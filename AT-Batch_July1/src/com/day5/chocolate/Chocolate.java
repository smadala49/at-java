package com.day5.chocolate;

public class Chocolate {
	private String companyName;
	private int barCode,weight,cost;
	private String name;
	
		Chocolate(){
			
	
}

		public String getCompanyName() {
			return companyName;
		}

		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}

		public int getBarCode() {
			return barCode;
		}

		public void setBarCode(int barCode) {
			this.barCode = barCode;
		}

		public int getWeight() {
			return weight;
		}

		public void setWeight(int weight) {
			this.weight = weight;
		}

		public int getCost() {
			return cost;
		}

		public void setCost(int cost) {
			this.cost = cost;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		
}