package com.day2;

public class Student {
		private String  name;
		private int age;
		private int yearOfJoining;
		
		public String getName() {
			return name;
}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public int getYearOfJoining() {
			return yearOfJoining;
		}

		public void setYearOfJoining(int yearOfJoining) {
			this.yearOfJoining = yearOfJoining;
		}

		public void setName(String name) {
			this.name = name;
		}
}