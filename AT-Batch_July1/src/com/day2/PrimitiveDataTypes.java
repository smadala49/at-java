package com.day2;

public class PrimitiveDataTypes {

	public static void main(String[] args) {
		
		int studentID=1234;
		boolean isPresent=false;
		double mathScore=95.87;
		char gender ='M';
		
		System.out.println("Studentid is "+studentID);
		System.out.println("IS Present "+isPresent);
		System.out.println("Match score is "+mathScore);
		System.out.println("Gender "+gender);

	}

}
